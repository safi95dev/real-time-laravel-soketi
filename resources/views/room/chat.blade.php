<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Room') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm ">
                <div class="overflow-hidden overflow-x-auto p-6 bg-white border-b border-gray-200">
                    <div class="min-w-full align-middle uppercase ">
                        room chat

                    </div>

                </div>
            </div>
            <div class="bg-white w-full rounded-b-2xl">

                <div id="messages_pool" class="h-80  p-3.5 space-y-2 overflow-y-scroll">

                </div>

                <form id="messageSubmit">
                    <label for="chat" class="sr-only">Your message</label>
                    <div class="flex items-center px-3 py-2 rounded-lg bg-gray-50 dark:bg-gray-700">
                        <button type="button"
                                class="inline-flex justify-center p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600">
                            <svg aria-hidden="true" class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M4 3a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V5a2 2 0 00-2-2H4zm12 12H4l4-8 3 6 2-4 3 6z"
                                      clip-rule="evenodd"></path>
                            </svg>
                            <span class="sr-only">Upload image</span>
                        </button>
                        <button type="button"
                                class="p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600">
                            <svg aria-hidden="true" class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-.464 5.535a1 1 0 10-1.415-1.414 3 3 0 01-4.242 0 1 1 0 00-1.415 1.414 5 5 0 007.072 0z"
                                      clip-rule="evenodd"></path>
                            </svg>
                            <span class="sr-only">Add emoji</span>
                        </button>
                        <textarea id="chat" rows="1"
                                  class="block mx-4 p-2.5 w-full text-sm text-gray-900 bg-white rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-800 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                  placeholder="Your message..."></textarea>
                        <button type="submit"
                                class="inline-flex justify-center p-2 text-blue-600 rounded-full cursor-pointer hover:bg-blue-100 dark:text-blue-500 dark:hover:bg-gray-600">
                            <svg aria-hidden="true" class="w-6 h-6 rotate-90" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z"></path>
                            </svg>
                            <span class="sr-only">Send message</span>
                        </button>
                    </div>
                </form>


            </div>
        </div>
    </div>

    <script>
        window.addEventListener('DOMContentLoaded', function () {
            let roomId = 6;
            let messagesPool = document.getElementById('messages_pool');
            let userID = "{{auth()->id()}}";
            let channel = Echo.join(`room.${roomId}`)

            channel.here((users) => {
                //
            }).joining((user) => {
                systemMessage(user.name, 'join');
            }).leaving((user) => {
                systemMessage(user.name, 'left');
            }).error((error) => {
                console.error(error);
            }).listen('SendMessage', function ({message, user}) {
                userMessage(message, user)
            });

            function scrollToBottom(element) {
                element.scroll({top: element.scrollHeight, behavior: 'smooth'});
            }

            function systemMessage(userName, type) {

                let message = `
                    <div class="w-full flex justify-center items-center">
                        <div class="${type === 'join' ? 'bg-gray-600' : 'bg-red-600'} w-fit text-white p-2 rounded-full text-xs">
                            ${userName} ${type === 'join' ? 'joined chat' : 'left chat'}
                        </div>
                    </div> `
                messagesPool.insertAdjacentHTML("beforeend", message);
                scrollToBottom(messagesPool);
            }

            function userMessage(text, user) {

                let message = `<div class="flex items-center ${user.id.toString() === userID.toString() ? 'flex-row-reverse' : ''}">

                        <div class="relative w-10 h-10 overflow-hidden bg-gray-100 rounded-full dark:bg-gray-600">
                            <svg class="absolute w-12 h-12 text-gray-400 -left-1" fill="currentColor"
                                 viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                                      clip-rule="evenodd"></path>
                            </svg>
                        </div>

                        <div class="p-2 mx-2 w-fit bg-slate-900 text-white rounded-br rounded-tl rounded-2xl">
                            <div class="text-xs">${user.name}</div>
                            <div class="">
                                ${text}
                            </div>
                        </div>



                    </div>`;

                messagesPool.insertAdjacentHTML("beforeend", message);
                scrollToBottom(messagesPool);

            }

            let messageSubmit = document.getElementById('messageSubmit');
            let chatInput = document.getElementById('chat');

            function submitOnEnter(event) {
                if (event.which === 13) {
                    if (!event.repeat) {
                        const newEvent = new Event("submit", {cancelable: true});
                        event.target.form.dispatchEvent(newEvent);
                    }

                    event.preventDefault(); // Prevents the addition of a new line in the text field
                }
            }

            chatInput.addEventListener("keydown", submitOnEnter);


            messageSubmit.addEventListener('submit', function (e) {
                e.preventDefault();
                console.log("message submited")


                let message = {
                    'room_id': roomId,
                    'text': chatInput.value,
                };


                const formData = new FormData();
                formData.append('message', message);
                chatInput.value = ''


                axios.post('/api/send-message', {message: message}).then(() => {
                    console.log('e', e)
                });
            });
        })
    </script>
</x-app-layout>