<?php

namespace App\Http\Controllers\Api;

use App\Events\SendMessage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SendingMessageController extends Controller
{
    public function __invoke(Request $request): \Illuminate\Http\Response
    {
        $message = $request->message;
//        return response()->json($message) ;

        broadcast(new SendMessage($request->user(),[
            'message' => $message,
        ]));

        return response()->noContent();
    }
}
