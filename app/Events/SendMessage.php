<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class SendMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected User $user;

    public  $message;


    public function __construct(User $user, array $payload)
    {
        $this->user    = $user;
        $this->message = Arr::pull($payload, 'message');
    }


    public function broadcastOn(): PresenceChannel
    {
//        return new PresenceChannel('room.'.$this->message['room_id']);
        return new PresenceChannel('room.'.$this->message['room_id']);
    }


    public function broadcastWith(): array
    {
        return [
            'user' => $this->user->only('id', 'name'),
            'message' => $this->message['text'],
        ];
    }

//
//    public function broadcastAs(): string
//    {
//        return 'room.message';
//    }
}
